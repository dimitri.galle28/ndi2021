<header>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color:#ef5330;">
        <div class="container-fluid">
            <a href="/views/easteregg.html"><img class="navbar-logo w-100" src="views/img/logo.png"></a>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" href="/">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/views/personnes.php">Personnes</a>
                        <div></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/views/sauvetage.php">Sauvetages</a>
                        <div></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/views/bateaux.php">Bateaux</a>
                        <div></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/views/stations.php">Stations</a>
                        <div></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/views/visualisation.php">visualisation</a>
                        <div></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/views/explication_cube_data.php">Explication cube data</a>
                        <div></div>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
</header>
