<footer class=".container-fluid  p-2 " style="color:white;">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js%22%3E"></script>

    <div class="row pt-3" style="background-color:black;">
        <div class="col">
            <div class="row">
                <div class="col"></div>
                <div class="col-6">
                    <h3>Plan du site</h3>
                    <ul>
                        <li class="nav-item">
                            <a style="color: white; text-decoration: none;" href="/">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a style="color: white; text-decoration: none;" href="/views/personnes.php">Personnes</a>
                        </li>
                        <li class="nav-item">
                            <a style="color: white; text-decoration: none;" href="/views/sauvetage.php">Sauvetages</a>
                        </li>
                        <li class="nav-item">
                            <a style="color: white; text-decoration: none;" href="/views/bateaux.php">Bateaux</a>
                        </li>
                        <li class="nav-item">
                            <a style="color: white; text-decoration: none;" href="/views/stations.php">Stations</a>
                        </li>
                        <li class="nav-item">
                            <a style="color: white; text-decoration: none;" href="/views/visualisation.php">visualisation</a>
                        </li>
                        <li class="nav-item">
                            <a style="color: white; text-decoration: none;" href="/views/explication_cube_data.php">Explication cube data</a>
                        </li>
                    </ul>
                </div>
                <div class="col"></div>
            </div>
        </div>
        <div class="col">
            <h3>Réseaux sociaux</h3>
            <div class="row mt-5 mb-5">
                <div class="col text-center">
                    <a href="https://www.facebook.com/groups/938396409644949">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="white" class="bi bi-facebook" viewBox="0 0 16 16">
                            <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
                        </svg>
                    </a>
                </div>
                <div class="col text-left">
                    <a href="https://twitter.com/boutelierphili1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="white" class="bi bi-twitter" viewBox="0 0 16 16">
                            <path d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z"/>
                        </svg>
                    </a>
                </div>
            </div>
            <p><a href="sauveteurdudunkerquois@gmail.com" style="color:white;text-decoration:none;">sauveteurdudunkerquois@gmail.com</a></p>
        </div>
        <div class="col">
            <div class="row">
                <h3>Newsletter</h3>
            </div>
            <div class="row">
                <input id="newsletter" class="mt-3" type="text">
            </div>
            <div class="row ">
                <input class="w-50 mt-3" type="button" onclick="" value="S'inscrire">
            </div>
        </div>
        <div class="col text-center">
            <img style="width:10rem;height:10rem;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Logo_de_la_Soci%C3%A9t%C3%A9_Nationale_de_Sauvetage_en_Mer_%28SNSM%29.svg/1024px-Logo_de_la_Soci%C3%A9t%C3%A9_Nationale_de_Sauvetage_en_Mer_%28SNSM%29.svg.png" alt="Logo SNSM">
        </div>
    </div>
    <div class = "row text-center" style="background-color:#041340;">
        <div class="col-md-4">Mentions Légales</div>
        <div class="col-md-4">Presse</div>
        <div class="col-md-4"><a style="color: white; text-decoration: none;" href="/views/credits.php">Crédits</a></div>
    </div>
</footer>
