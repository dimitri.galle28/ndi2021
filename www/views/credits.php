<html>
<?php require_once "head.php";?>
<?php require_once "header.php";?>
<body>
<div class="container">
    <div class="row my-3">
<div class="col-md-12 text-center">
        <h2 id="participants">Participants :</h2>
        <pre><code>
Clement LEGROUX
Dimitri GALLE
Taha BOUCHIKHI
Lucas CARRILHO GOMES
Maximilien PONCET
Florian MARCOT
Ludovic MATHURIN-CAYOL
Alexandre MOREAUX &lt;3
Dylan TOLEDANO
Paul RITTAUD
Nicolas MOUCHET-PETIT
Said Mohammad ZUHAIR
Hugo CHRETIEN
Gabriel TORTET
Segolene LAZARUS
Estéban JOUAN
Manon CATTANEO
Romaissa BERREKAM
Nasser AZROU-ISGHI
Samih BEZAA
Lucile JACQUEMART
Romuald BRIE
Taha BOUCHIKHI
Nikola GANDON
Heloise LECHELLE
Antonin TILLIER
Noé MATUSZENSKI
Oscar PLAISANT
Yanis SOURIOU
Mohammed BENABDILLAH
Yanis DEZZAZ
Marc SANCHEZ
Arthur PERREAU
et nos profs préférés : Adam & Maëlle
            </code></pre>
        <p><strong>Université De Tours - Antenne de Blois</strong></p>
        <h3 id="remerciements">Remerciements :</h3>
        <p>Aux encadrant pour nous avoir supportés pendant la nuit <code>&lt;3</code></p>
        <p>Aux organisateurs de <a href="https://nuitdelinfo.com">la nuit de l’informatique</a>.</p>
        <p>A Gargamour pour sa participation caché, fil rouge de cette nuit et son potit chat Migrou !</p>
        <p>A la machine à café ( <em>Stonks</em> ).</p>

    </div>
    </div>

</div>
</body>
<?php require_once "footer.php" ?>
</html>