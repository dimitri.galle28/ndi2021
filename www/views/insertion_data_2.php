<?php include('header.php'); ?>
<?php include('footer.php'); ?>
<?php
include('connect.php');
if(empty($_POST['inputSauveteur'] || $_POST['inputVictimes'] || $_POST['inputStation'])) {
    echo 'erreur';
}
else {
    $inputSauveteur = $_POST['inputSauveteur'];
    $inputVictimes = $_POST['inputVictimes'];
    $inputStation = $_POST['inputStation'];

    $result = pg_prepare($db, "INSERT INTO fait_sauvetage (sauveteur, victimes, station) VALUES ($inputSauveteur, $inputVictimes, $inputStation)");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<form method="POST">
  <div class="form-group row">
    <label for="inputSauveteur" class="col-sm-2 col-form-label">Sauveteur</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputSauveteur" placeholder="Sauveteur">
    </div>
  </div>
  <form>
  <div class="form-group row">
    <label for="inputVictimes" class="col-sm-2 col-form-label">Nombre de victimes</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputVictimes" placeholder="Nombre de victimes">
    </div>
  </div>
  <form>
  <div class="form-group row">
    <label for="inputStation" class="col-sm-2 col-form-label">Station</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputStation" placeholder="Station">
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">InsÃ©rer</button>
    </div>
  </div>
</form>
</body>
</html>
