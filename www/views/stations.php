<html>
<?php require_once "head.php";?>
<?php require_once "header.php";?>
<body>
<div class="container-fluid mb-3 ">

    <div class="station row mt-3 py-5" style="background-color:#4BA4D4">
        <div class="col-4">
            <img class="photoStation" src="img/Dunkerque.png" width="200" height="250"> <!-- a remplacer par la requete sql-->
        </div>
        <div class="col">
            <h1> nom station</h1>

            <p> petit description de la ville ou station </p>

        </div>
    </div>
</div>

<div class="container">
    <div class="dates row pt-3 px-3">
        <div class="col-4" style="background-color:#33B7FF">
            <p>Date de création :</p> <!-- requete -->
        </div>
        <div class="col-8">

        </div>
    </div>

    <div class="dates row px-3 pb-3 ">
        <div class="col" style="background-color:#33B7FF">
            La station de <!-- mettre la requete nom--> a été ouverte <!-- requete de fermeture -->      </div>
    </div>

    <!-- pareil en dessous, faudra lefaire en php avec une petite fonction-->
    <div class="dates row pt-3 px-3">
        <div class="col-4" style="background-color:#33B7FF">
            <p>Date de fermeture   :</p> <!-- requete -->
        </div>
        <div class="col-8">

        </div>
    </div>

    <div class="dates row px-3 pb-3 ">
        <div class="col" style="background-color:#33B7FF">
            La station de <!-- mettre la requete nom--> a été fermé <!-- requete de fermeture -->
        </div>
    </div>

</div>
</body>
<?php require_once "footer.php" ?>
</html>
