<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Explication de la structure de la base de données</title>
</head>
<?php require_once "header.php";?>
<body>
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Explication de la structure de la base de données</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="structure_bd.PNG" alt="Structure base de données">
        </div>
    </div>
</body>
<?php require_once "footer.php";?>
</html>
