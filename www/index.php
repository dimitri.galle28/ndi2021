<html>
    <?php require_once "views/head.php";?>
    <?php require_once "header.php";?>
    <body>
    <div class="container-fluid mb-3 ">

        <div class="row" style="background-color:#FFFFFF">
            <div class="col-3">
                <img src="views/img/bouer.svg" >
            </div>
            <div class="col-9 my-auto">
                <p class="text-center">Bienvenue sur le site des sauveteurs du dunkerquois. Ce site rend hommage aux femmes, hommes et enfants qui ont réalisé des actes de sauvetages en milieu aquatique.
                    Ces sauveteurs, habitants du dunkerquois (de Bray-Dunes à Grand-Fort-Philippe), ont participé à plus de 900 sauvetages en mer et plus de 1100 sauvetages individuels.
                    Œuvrant avec courage, abnégation et souvent au mépris du risque ils méritent amplement que leurs actes soient pérennisés.</p>
            </div>
        </div>
        <div class="container-fluid mb-3 ">

            <div class="row" style="background-color:#4BA4D4">
                <div class="col-9 my-auto">
                    <p class="text-center">Si tous nos sauveteurs n’ont pas eu la Légion d’Honneur, ils n’en ont pas moins mérité dans leurs multiples  sorties en mer ou dans leurs actes individuels.

                        La Société Humaine de Dunkerque, La Société Centrale de Sauvetage des Naufragés, la Société Nationale de Sauvetage en Mer, les pays étrangers, le Ministère de la Marine ou de l’Intérieur, l’Académie Française et bien d’autres  ont su les remercier en délivrant médailles, prix, objets et récompenses diverses  en fonction des actes réalisés.

                        Cette section comporte des fiches synthétiques sur les patronymes du  sauvetage – 1 page par patronyme quel que soit le nombre d’occurrences. Cette section ne peut s’alimenter qu’avec vos apports (Documents photos etc..)

                        Le patronyme peut pointer sur une fiche personnelle – L’éventuelle fiche patronymique est en attente

                        Les marins qui auront rempli les fonctions de sous-patron voire patron du canot de sauvetage  de manière ponctuelle seront aussi cités.
                    </p>

                </div>
                <div class="col-3 mt-3">
                    <img src="views/img/mouette.svg" >
                </div>
            </div>
            <div class="container-fluid mb-3 ">

                <div class="row" style="background-color:#FFFFFF">
                    <div class="col-3">
                        <img src="views/img/balise.svg" >
                    </div>
                    <div class="col-9 my-auto">
                        <p class="text-center">Bienvenue sur le site des sauveteurs du dunkerquois. Ce site rend hommage aux femmes, hommes et enfants qui ont réalisé des actes de sauvetages en milieu aquatique.
                            Ces sauveteurs, habitants du dunkerquois (de Bray-Dunes à Grand-Fort-Philippe), ont participé à plus de 900 sauvetages en mer et plus de 1100 sauvetages individuels.
                            Œuvrant avec courage, abnégation et souvent au mépris du risque ils méritent amplement que leurs actes soient pérennisés.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php require_once "views/footer.php" ?>
</html>
